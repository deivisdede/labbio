<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Contato</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="contact">
		<div id="page-wrapper">

		<!-- Header -->
			<?php
				include 'script.html'
			?>
			
			<?php
				include 'header.html'
			?>

			<!-- Main -->
				<article id="main">

					<header class="special container">
						<span class="icon fa-codepen"></span>
						<h2>3D PRITING</h2>
						<p>Contact us and ask for a budget.</p>
					</header>

					<!-- One -->
						<section class="wrapper style4 special container 75%">

							<!-- Content -->
								<div class="content">
									<form>
										<div class="row 50%">
											<div class="6u 12u(mobile)">
												<input type="text" name="name" placeholder="Name" />
											</div>
											<div class="6u 12u(mobile)">
												<input type="text" name="email" placeholder="Email" />
											</div>
										</div>
										<div class="row 50%">
											<div class="12u">
												<input type="text" name="subject" placeholder="Description" />
											</div>
										</div>
										<div class="row 50%">
											<div class="12u">
												<textarea name="message" placeholder="Message" rows="7"></textarea>
											</div>
										</div>
										<div class="row 50%">
											<div class="12u">
												 <label for="file-upload" class="custom-file-upload">
												    <i class="fa fa-cloud-upload"></i> Upload Zip File
												</label>
												<input id="file-upload" name="file" type="file" accept=".zip" />
											</div>
										</div>
										<div class="row">
											<div class="12u">
												<ul class="buttons">
													<li><input type="submit" class="special" value="Submeter" /></li>
												</ul>
											</div>
										</div>
									</form>
								</div>

						</section>

				</article>
			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>

	</body>
</html>