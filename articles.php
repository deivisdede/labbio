<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Research - LABBIO</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>

			<!-- Main -->
				<article id="main" style="padding-top: 18%;">		
					

					<!-- Articles -->
					<div class="container aarticles">
						<?php articles(); ?>						
					</div>

			</article>

			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>
<?php
	function articles(){

		if (($handle = fopen("./adm-sheets/articles.csv", "r")) !== FALSE) {
			$indTitle = 2;
			$indYear = 0;
			$indAuthor = 1;
			$indLink = 4;

			

		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		    
		    	
		        $str = '<div class="row">
				        	<div class="aarticle 6u 12u(mobile)">
								<div class="uarticle">
									<div class="row">
										<div class="11u title">
											<p><strong>'.$data[$indTitle].'</strong></p>
										</div>
									</div>
									<div class="row">
										<div class="5u">
											<p><strong>Author: </strong> '.$data[$indAuthor].'</p>
										</div>
										<div class="3u">
											<p><strong>Year: </strong> '.$data[$indYear].'</p>
										</div>
										<div class="4u">
											<p><strong>Link:</strong> <a href="'.$data[$indLink].'">click</a></p>
										</div>
									</div>
								</div>
							</div>';
			if(($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$str = $str.'
							<div class="aarticle 6u 12u(mobile)">
								<div class="uarticle">
									<div class="row">
										<div class="11u title">
											<p><strong>'.$data[$indTitle].'</strong></p>
										</div>
									</div>
									<div class="row">
										<div class="5u">
											<p><strong>Author: </strong> '.$data[$indAuthor].'</p>
										</div>
										<div class="2u">
											<p><strong>Year: </strong> '.$data[$indYear].'</p>
										</div>
										<div class="4u">
											<p><strong>Link:</strong> <a href="'.$data[$indLink].'">click</a></p>
										</div>
									</div>
								</div>
							</div>
						';
			}

			$str = $str.'</div>';

			echo $str;

		
		}

		fclose($handle);
	}
}
?>

	</body>
</html>