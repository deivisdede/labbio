<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Contact-us - LABBIO</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="contact" id="contact">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>

			<!-- Contact form -->
				<header class="special container" style="top: 7em !important;">
					<span class="icon fa-envelope"></span>
					<h2>Get In Touch</h2>
					<p>Any questions? Ideas? Or just want to talk? Contact-us!</p>
				</header>
				<?php
					include "contact-form.html"
				?>

			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>


	</body>
</html>