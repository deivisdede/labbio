<!DOCTYPE html>
<html>

<?php 

	echo '<div class="content" style="width: 90%; padding: 0; margin: 5% auto; text-align: center;">
				
		<h3>You can filter the team here!</h3>
		<form action="./meet-the-team.php">
			<label for="filter_type"></label>
			<select name="filter_type" id="filter_type" class="select" style="display: block !important; font-weigth: bold;" onchange="loadItemList()">
				<option></option>
				<option value="actuation">Actuation</option>
				<option value="position">Position</option>
				<option value="relationship">Relationship</option>
			</select>';
	echo '<div id="filter_options"> ';
		loadList("adm-sheets/team.csv");
	echo '</div>';


?>

<script>
function loadItemList() {
    var x = document.getElementById("filter_type").value;
    document.getElementById("actuation").classList.remove("show")
    document.getElementById("position").classList.remove("show")
    document.getElementById("relationship").classList.remove("show")
    document.getElementById(x).classList.add("show");
    //var uniqueItems = Array.from(new Set(items))
}
</script>	

</html>

<?php
	function loadList($file){
		for($i = 1; $i <= 3; $i++){
			if($i == 1){
				$filter_type = "actuation";
			} else if($i==2){
				$filter_type = "position";
			}else if($i==3){
				$filter_type = "relationship";

			}

			if($filter_type === "actuation"){
				$col = 6;
				$col2 = 7;

			} else if($filter_type === "position"){
				$col = 5;
				$col2 = 5;
			} else if($filter_type === "relationship"){
				$col = 1;
				$col2 = 1;

			}
			
			echo 
			'<style>
				.show{
					display: block !important;
				} 
				.select{
					display:none;
					border: 1px solid #caced0;
				    padding: 6px 20px;
				    width: 60%;
				    text-align: center;
				    margin: 2% auto;
				    background: none;
				}
			</style>';
						

			echo '<select name ="'.$filter_type.'" id="'.$filter_type.'" class="dropdown-content select">';	
			if (($handle = fopen($file, "r")) !== FALSE) {

				$list = array();
			    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			    	array_push($list, $data[$col]);
			    	array_push($list, $data[$col2]);
				}
				$list = array_unique($list);
				
					foreach ($list as $option) {
						if($option !== "#")
						echo '<option value="'.$option.'">'.$option.'</option>';
					}
			    fclose($handle);
			}
			echo '</select>';
		}
	}
	
?>