<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>History - LABBIO</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="index">
		<div id="page-wrapper">

			<!-- Header -->
			<?php
				include 'script.html'
			?>
			
			<?php
				include 'header.html'
			?>
			<!-- Banner -->
			<section id="banner" style="padding: 10%;">
				<div class="inner" style="text-align: justify;">

					<h2 style="text-align: center;" id="history">
					The <strong>History</strong> of Our
					<strong>lab</strong></h2><br/><br/>
					<p>In July of 1999, was founded by Professor Marcos Pinotti Barbosa, the Laboratory of Bioengineering (LABBIO) of the Federal University of Minas Gerais.<br/><br/></p>

					<p>Since that time, LABBIO has formed about 10 postdoctors, 30 doctors and more than 50 masters. Many undergraduates made the first contact with a scientific activity through LABBIO. Currently, LABBIO counts on a participation of 3 postdoctoral students, 8 doctoral students, 5 masters and 16 graduates. <br/><br/></p>

					<p>Since the foundation, LABBIO has produced about 100 scientific papers and twenty three patent applications. The main research areas are cardiovascular engineering, biomaterials, methods of laser therapy and cryotherapy, rehabilitation engineering, methods of diagnosis and therapy in dentistry, neurovision, safety engineering, plastic surgery, cardiac surgery, laser in dentistry, sports biomechanics.<br/><br/></p>

					<p>The research is conduct by a multidisciplinary team gathered from health science (medicine, physiotherapy, occupational therapy, pharmacy, dentistry, and physical education) engineering fields (mechanical, electrical, chemical and mechatronics).<br/><br/></p>

					<p>Besides the commitment of all the members, it is many years of experience to obtain a quality knowledge with the potential for the development of society.</p>

				</div>

			</section>

			
			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>

	</body>
</html>