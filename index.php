<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>LabBio UFMG</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="index">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>
				<script type="text/javascript">

					function goToMain(){
						//window.location.href = '#main';	
						var pos = (((document.getElementById("main")).getBoundingClientRect()).top);
						var body = $("body, html");
						body.animate({scrollTop :pos*1.01}, 500,function(){
				      });

					}
					function pauseVideo(){
						vid = document.getElementById("vid");
						vid.pause();
					}
					function playVideo(){
						vid = document.getElementById("vid");
						vid.play();
					} 
				</script>

			<!--intro video-->

			<!-- Banner -->
				<section id="banner" style="margin: 0 auto; padding: 0;">
					
					<!--
						".inner" is set up as an inline-block so it automatically expands
						in both directions to fit whatever's inside it. This means it won't
						automatically wrap lines, so be sure to use line breaks where
						appropriate (<br />).

					-->
					<video id="vid" width="100%" height="5%" autoplay loop muted onmouseleave="pauseVideo()" onmouseenter="playVideo()" onseeked="goToMain()">
					  <source src="videos/intro.mp4" type="video/mp4">
					</video>

					<!-- <div class="inner">

						<header>
							<h2>LABBIO</h2>
						</header>
						<p><strong>ENGINEERING</strong>
						<br />
						supporting
						<br />
						<strong>LIVES</strong></p>
						<footer>
							<ul class="buttons vertical">
								<li><a href="./overview.php" class="button fit scrolly">ABOUT US</a></li>
							</ul>
						</footer>

					</div> -->

				</section>

			<!-- Main -->
				<article id="main">

					<header class="special container">
						<span class="icon fa-flask"></span>
						<h2>There are three <strong>GOALS</strong> that we follow<br /></h2>
					</header>
					
					<section class="wrapper style1 container special">
							<div class="row">
								<div class="4u 12u(narrower)">

									<section>
										<span class="icon featured fa-check"></span>
										<header>
											<h3>INNOVATION</h3>
										</header>
										<p>Translate the academic excellence into <strong>innovations</strong> available to industry.</p>
									</section>

								</div>
								<div class="4u 12u(narrower)">

									<section>
										<span class="icon featured fa-check"></span>
										<header>
											<h3>DEVELOPMENT</h3>
										</header>
										<p>Develop <strong>high quality</strong> scienfic research in the field of Biomedical Engineering and Biomimetics.</p>
									</section>

								</div>
								<div class="4u 12u(narrower)">

									<section>
										<span class="icon featured fa-check"></span>
										<header>
											<h3>IMPROVEMENT</h3>
										</header>
										<p>Make available to the clinical partners useful engineering tools and new developments to <strong>improve</strong> the patients' <strong>life quality</strong>.</p>
									</section>

								</div>
								
							</div>
						</section>


					<!-- One -->
						<section class="wrapper style2 container special-alt">
							<div class="row 50%">
								<div class="8u 12u(narrower)">

									<header>
										<h2 style="text-align: justify;">The application of <strong>engineering</strong> to <strong>biological systems</strong> can make life so much better!</h2>
									</header>
									<p style="text-align: justify !important;">We believe that! That's why our <strong>researchs</strong> provides the usage of engineering 
										 knowledge to biotechnologies in order to develop new resources that make the world  better to live. The main research areas are biomechanics and assistive technology, cardiovascular engineering, photobiomodulation and photodynamic therapy, biomimetics and medical devices (gynecology, cardiovascular surgery, dermatology and dentistry). </p>
									<footer>
										<ul class="buttons">
											<li><a href="#researchs" class="button">Find Out More</a></li>
										</ul>
									</footer>

								</div>
								<div class="4u 12u(narrower) important(narrower)">


									<ul class="featured-icons">
										<li><span class="icon fa-assistive-listening-systems"><span class="label">Feature 1</span></span></li>
										<li><span class="icon fa-cog"><span class="label">Feature 2</span></span></li>
										<li><span class="icon fa-heartbeat"><span class="label">Feature 3</span></span></li>
										<li><span class="icon fa-laptop"><span class="label">Feature 4</span></span></li>
										<li><span class="icon fa-user-md"><span class="label">Feature 5</span></span></li>
										<li><span class="icon fa-medkit"><span class="label">Feature 6</span></span></li>
									</ul>

								</div>
							</div>
						</section>

					<!-- Two -->
						
					<!-- Three -->
						<section id="researchs" class="wrapper style3 container special">

							<header class="major">
								<h2>Our <strong>Researchs</strong></h2>
							</header>

							<div class="row">
								<div class="6u 12u(narrower)">

									<section>
										<a  class="image featured"><img src="images/biomechanics.jpg" alt="" /></a>
										<header>
											<h3>BIOMECHANICS</h3>
										</header>
										<p>Understanding the forces acting on the joints of the human body is essential for the improvement of rehabilitation procedures and surgery.</p>
									</section>

								</div>
								<div class="6u 12u(narrower)">

									<section>
										<a  class="image featured"><img src="images/photobiomodulation.jpg" alt="" /></a>
										<header>
											<h3>PHOTOBIOMODULATION</h3>
										</header>
										<p>The photobiomodulation consists of electromagnetic waves in the spectral range from red to near infrared to stimulate cellular function by promoting therapeutic effects.</p>
									</section>

								</div>
							</div>
							<div class="row">
								<div class="6u 12u(narrower)">

									<section>
										<a  class="image featured"><img src="images/assistive-technology.jpg" alt="" /></a>
										<header>
											<h3>ASSISTIVE TECHNOLOGY</h3>
										</header>
										<p>The area of Assistive Technology comprises the development, transfer and dissemination of technologies to help provide or enhance functional abilities of people with disabilities or reduced mobility.</p>
									</section>

								</div>
								<div class="6u 12u(narrower)">

									<section>
										<a  class="image featured"><img src="images/cardiovascular-biomechanics.jpg" alt="" /></a>
										<header>
											<h3>CARDIOVASCULAR BIOMECHANICS</h3>
										</header>
										<p>Bioengineering is the application of engineering concepts and techniques in order to design or adapt equipment for living organisms.</p>
									</section>

								</div>
							</div>
							<div class="row">
								<div class="6u 12u(narrower)">

									<section>
										<a  class="image featured"><img src="images/biomimetic.jpg" alt="" /></a>
										<header>
											<h3>BIOMIMETIC</h3>
										</header>
										<p>Biomimetics is the study of the structure and function of biological systems as models for the design and engineering of materials and machines. 
										</p>
									</section>

								</div>
								<div class="6u 12u(narrower)">

									<section>
										<a  class="image featured"><img src="images/photobiomodulation.jpg" alt="" /></a>
										<header>
											<h3>3D IMPRESSION</h3>
										</header>
										<p>To accelerate ours innovation process, we use 3D Printing to build ours prototypes. Also, to make lives better, we use ours techniques to bioprinting human tissue.</p>
									</section>

								</div>
							</div>
							<div class="row">
								<div class="6u 12u(narrower)">

									<section>
										<a  class="image featured"><img src="images/photodynamic.jpg" alt="" /></a>
										<header>
											<h3>PHOTODYNAMIC THERAPY</h3>
										</header>
										<p>Photodynamic therapy, or PDT, is a therapy where a photosensitive compound (dye) is administered to the patient and absorbed by bacteria, fungi and other microorganism abusers.</p>
									</section>

								</div>
								<div class="6u 12u(narrower)">

									<section>
										<a  class="image featured"><img src="images/medical-devices.jpg" alt="" /></a>
										<header>
											<h3>MEDICAL DEVICES</h3>
										</header>
										<p>Understanding the forces acting on the joints of the human body is essential for the improvement of rehabilitation procedures and surgery.</p>
									</section>

								</div>
							</div>

							<footer class="major">
								<ul class="buttons">
									<li><a href="products.php" class="button">See More</a></li>
								</ul>
							</footer>

						</section>

				</article>

			<!-- CTA -->
				<section id="cta">

					<header>
						<h2>Want to <strong>work</strong> with us?</h2>
						<p>If you have a interesting work or if you are thinking about a partnership, you can come to us and talk.</p>
					</header>
					<footer>
						<ul class="buttons">
							<li><a href="location.php" class="button special">Make a Visit</a></li>
							<li><a href="contact.php" class="button">Get In Touch</a></li>
						</ul>
					</footer>

				</section>

			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>


	</body>
</html>