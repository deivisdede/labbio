<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Our location - LABBIO</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>

			<!-- Main -->
				<article id="main">

					<header class="special container">
						<span class="icon fa-map-marker"></span>
						<h2><strong>Bioengineering</strong> Laboratory</h2>
						<p>Departamento de Engenharia Mecânica da UFMG</p>
						<p>Av. Pres. Antônio Carlos, 6627 - Pampulha, Belo Horizonte - MG, 31270-901</p>
					</header>

					<!-- One -->
						<section class="wrapper style4 container" style="padding-top: 1px;">

							<!-- Content -->
								<div class="content">
									<section>
										<a href="#" class="icon circle fa-map-marker-alt"><span class="label">Maps</span></a>
											<div id="googleMap" style="width:100%;height:600px; margin-bottom: 40px;"></div>

											<script>
											function myMap() {
											var mapProp= {
											    center:new google.maps.LatLng(-19.870463, -43.961977),
											    zoom:18,
											};
											var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
											}
											</script>

											<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe_hzMN_kxpNHB3ubhtbpqiTZPIcTfDfk&callback=myMap"></script>

									</section>
								</div>
								<footer class="major">
									<ul class="buttons"  style="margin: 0 auto; text-align: center;">
										<li><a href="contact.php" class="button">Contact-us</a></li>
									</ul>
								</footer>

						</section>


				</article>

			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>

	</body>
</html>