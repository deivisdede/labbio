<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Maintenance - LABBIO</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>

			<!-- Main -->
				<article id="main">

					<header class="special container">
						<span class="icon fa-cogs	"></span>
						<h1>Our website is currently undergoing essential maintenance! </h1>
						<h2> We'll be back online shortly.</h2>
					</header>

					<h1 style="text-align: center;">If you have any questions please contact our team!</h1>
					
					

					<!-- One -->
						<section class="wrapper style4 container" style="padding-top: 1px;">

							<!-- Content -->
								<footer class="major">
									<ul class="buttons"  style="margin: 0 auto; text-align: center;">
										<li><a href="contact.php" class="button">Contact-us</a></li>
									</ul>
								</footer>

						</section>


				</article>

			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>

	</body>
</html>