<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<?php
	$areaAtuacao= isset($_GET['areaAtuacao']) ? $_GET['areaAtuacao']: 'oi';
	$areaAtuacao = urldecode($areaAtuacao);
?>
<html>
	<head>
		<title>Our Team - LABBIO</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>

			<!-- Main -->
				<article id="main">

					<header class="special container">
						<span class="icon fa-users"></span>
						<div style="width: 80%; display: block; margin: 0 auto;">
							<h2>Coming <strong> together </strong>   is a beginning.</h2>

							<h2>Keeping <strong>together</strong> is a progress.</h2>

							<h2>Working <strong>together</strong> is success.</h2>
							<p style="text-align: right; padding-right: 20%;">Henry Ford</p>
						</div>

							<h2 style="padding-top: 70px; border-top: solid 1px #caced0; margin-top: 40px; margin-bottom: 1px;"><strong> MEET OUR TEAM </strong></h2>
					</header>

					<!-- One -->

						<section class="wrapper style4 container" style="border-top: solid 1px #caced0;">

							<!-- Content -->
							<div class="content" style="width: 90%; padding: 0; margin: 0 auto;">
								<section style="width: 100%; padding: 0; margin: 0 auto;">
									<?php
										team();
									?>

								</section>
							</div>
							<div class="content" style="width: 90%; padding: 0; margin: 5% auto; text-align: center;">
							
							<h3></h3>
							<form action="./meet-the-team.php">
								  <select name="areaAtuacao" class="select-areaAtuacao">

								    <option value="Todas">Todas</option>
								    <option value="Engenharia Biomédica">Engenharia Biomédica</option>
								    <option value="Clínica">Clínica</option>
								    <option value="Desenvolvimento de Técnicas e Equipamentos para Saúde Humana e Animal">Desenvolvimento de Técnicas e Equipamentos para Saúde</option>
								    <option value="Impressão 3D">Impressão 3D</option>
								    <option value="Assessoria">Assessoria</option>
								    <option value="Engenharia Cardiovascular">Engenharia Cardiovascular</option>
								    <option value="Terapia Fotodinâmica e Fotobiomodulação">Terapia Fotodinâmica e Fotobiomodulação</option>
								    <option value="Tecnologia Assistiva">Tecnologia Assistiva</option>
								    <option value="Engenharia Biomecânica">Engenharia Biomecânica</option>
								    <option value="Ergonomia">Ergonomia</option>
								    
								  </select>
								  <br><br>
								  <input type="submit" value="FILTER">
								</form>
							</div>

						</section>


				</article>

			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>
<?php
	function team(){

		$row = 1;
		if (($handle = fopen("./adm-sheets/team.csv", "r")) !== FALSE) {
			$indNome = 3;
			$indVinculo = 1;
			$indImg = 8;
			$indCargo = 5;
			$indAtuacao1 = 6;
			$indAtuacao2 = 7;
			$indPesquisa = 2;
			$indMail = 4;
			$indLattes = 9;
			$indLinkedIn = 10;

			echo "<div>";
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		        $num = count($data);
		        global $areaAtuacao;
		        
		        if(!isset($_GET['areaAtuacao']) or (isset($_GET['areaAtuacao']) and( ("Todas" === str_replace("+", " ", $areaAtuacao)) or ($data[$indAtuacao1] === str_replace("+", " ", $areaAtuacao)) or ($data[$indAtuacao2] === str_replace("+", " ", $areaAtuacao)) )))
		        {
		       		
		    	
			        echo "<div class='div-team'>";
				          
			            echo "<p class='p-team'><strong>" . $data[$indNome] . "</strong></p>";
			            echo '<img class="img-team" width="120" height="120" src="./profile_pictures/'. $data[$indImg] .'">';
			            /*echo "<p class='p-team'>". $data[$indAtuacao1] ."</p>";*/
			            echo '<a style="margin: 1px; padding; 1px;" href="team-member.php?nome='.str_replace(" ", "+", $data[$indNome]).'"class="button">See More</a>';

				    echo "</div>";
		    	}
			}
		    echo "</div>";
		    fclose($handle);
		}
	}
?>
	</body>
</html>