<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Research - LABBIO</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>

			<!-- Main -->
				<article id="main" style="padding-top: 18%;">		
					

					<!-- Articles -->
					<div class="container aarticles">
						<div class="row">
							<div class="aarticle 6u 12u(mobile)">
								<div class="uarticle">
									<div class="row">
										<div class="11u title">
											<p><strong>SISTEMA DE TRIAGEM VISUAL E AUDITIVA DE CRIANÇAS EM IDADE ESCOLAR, CONECTADO A UM BANCO DE DADOS</strong></p>
										</div>
									</div>
									<div class="row">
										<div class="5u">
											<p><strong>Author: </strong> Fabrício Carvalho Soares</p>
										</div>
										<div class="5u">
											<p><strong>Year: </strong> 2009</p>
										</div>
									</div>
									<div class="row">
										<div class="10u" >
											<p class="abstract"><strong>Abstract: </strong> Diferentes programas de triagem da acuidade visual em crianças com idade escolar já foram realizados. Na maioria destes programas pode-se observar que, apesar dos bons resultados, falta um sistema adequado para a realização dos exames, armazenamento dos dados e o seu tratamento estatístico em larga escala. O objetivo deste trabalho é desenvolver um sistema de exames, acoplado a um banco de dados, que possa ser aplicado em programas públicos de triagem. Neste sistema estão acoplados subsistemas para a realização de diversos exames relacionados à acuidade visual, ao exame do limiar auditivo e para a transmissão automática dos dados para um computador/ servidor. O sistema de triagem conseguiu atingir os objetivos propostos, podendo ser DESENVOLVIMENTO DE UM SISTEMA DE TRIAGEM DA ACUIDADE VISUAL E DO LIMIAR AUDITIVO ACOPLADOS EM UM BANCO DE DADOS DEVELOPMENT OF A VISUAL ACUITY AND HEARING THRESHOLD SCREENING SYSTEM utilizado como uma ferramenta capaz de auxiliar nos exames de acuidade visual com letras, “E”, “C” e números, exames de foria, de sensibilidade ao contraste, de visão de cores (Ishihara) e do limiar auditivo. Também foi capaz de armazenar, de maneira automá- tica, os dados em um servidor central, sem a intervenção do examinador. PALAVRAS-CHAVE: acuidade visual, triagem, limiar auditivo, armazenamento de dados.
											</p>
										</div>
									</div>
									<div class="row">
										<div class="10u">
											<ul><a  class="button" style="margin: 0 auto; display: block; width: 20%;" href="#">See More</a></ul>
										</div>
									</div>
								</div>
							</div>
							<div class="aarticle 6u 12u(mobile)">
								<div class="uarticle">
									<div class="row">
										<div class="11u title">
											<p><strong>SISTEMA DE TRIAGEM VISUAL E AUDITIVA DE CRIANÇAS EM IDADE ESCOLAR, CONECTADO A UM BANCO DE DADOS</strong></p>
										</div>
									</div>
									<div class="row">
										<div class="4u">
											<p><strong>Author: </strong> Fabrício Carvalho Soares</p>
										</div>
										<div class="4u">
											<p><strong>Year: </strong> 2009</p>
										</div>
										<div class="4u">
											<p><strong>Link:</strong> <a href="#">click<a></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
						


				</article>

			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>
<?php
	function articles(){

		if (($handle = fopen("./adm-sheets/articles.csv", "r")) !== FALSE) {
			$indTitle = 1;
			$indYear = 0;
			$indAuthor = 2;
			$indLink = 3;

			$i = 0;

		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		    	

		    	if($i%2 == 0){
		    		echo '<div class="row">';
		    	}
		        $num = count($data);
		    	$i++;
		    	
		        echo '<div class="aarticle 6u 12u(mobile)">
							<div class="uarticle">
								<div class="row">
									<div class="11u title">
										<p><strong>'.$data[$indTitle].'</strong></p>
									</div>
								</div>
								<div class="row">
									<div class="4u">
										<p><strong>Author: </strong> '.$data[$indAuthor].'</p>
									</div>
									<div class="4u">
										<p><strong>Year: </strong> '.$data[$indYear].'</p>
									</div>
									<div class="4u">
										<p><strong>Link:</strong> <a href="'.$data[$indLink].'">click<a></p>
									</div>
								</div>
							</div>
						</div>';
	
			}

			if($i%2 == 0){
		    		echo '</div>';
		    	}
		    fclose($handle);
		}
	}
?>

	</body>
</html>