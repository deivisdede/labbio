<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Overview - LABBIO</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->

	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>
				<?php
					include 'carousel.php'
				?>

			<!-- Main -->
				<article id="main">

					<!-- TEXTO INTRODUTORIO -->
					<!-- TEXTO INTRODUTORIO -->
					<!-- TEXTO INTRODUTORIO -->

					<section class="wrapper style1 container special">
							<header class="major">
								<h2>The <strong>BIOENGINEERING</strong> lab</h2>
							</header>
							<div class="row">
								<div class="4u 12u(narrower)">

									<section>
										<span class="icon featured fa-check"></span>
										<header>
											<h3>INNOVATION</h3>
										</header>
										<p>Translate the academic excellence into <strong>innovations</strong> available to industry.</p>
									</section>

								</div>
								<div class="4u 12u(narrower)">

									<section>
										<span class="icon featured fa-check"></span>
										<header>
											<h3>DEVELOPMENT</h3>
										</header>
										<p>Develop <strong>high quality</strong> scienfic research in the field of Biomedical Engineering and Biomimetics.</p>
									</section>

								</div>
								<div class="4u 12u(narrower)">

									<section>
										<span class="icon featured fa-check"></span>
										<header>
											<h3>IMPROVEMENT</h3>
										</header>
										<p>Make available to the clinical partners useful engineering tools and new developments to <strong>improve</strong> the patients' <strong>life quality</strong>.</p>
									</section>

								</div>
								
							</div>
						</section>

						<!-- ALGUMAS PESQUISAS -->
						<!-- ALGUMAS PESQUISAS -->
						<!-- ALGUMAS PESQUISAS -->

						<!-- PROFESSORES -->
						<!-- PROFESSORES -->
						<!-- PROFESSORES -->

					<!-- One -->
						<section class="wrapper style2 container special-alt">
							<div class="row 50%">
								<div class="8u 12u(narrower)">

									<header>
										<h2 style="text-align: justify;"> In a warehouse, the <strong>BioEngineering Lab</strong> was founded, in July, 1999.</h2>
									</header>
									<p style="text-align: justify !important;">When Marcos Pinotti arrived at UFMG, in 1999, the lack of space didn't stop him: his team, formed by some PHD students, emptied the warehouse and turned it into the essence of the Bioengineering Laboratory, which today occupies a much more spacious area in the building of Mechanical Engineering.
									Inventor, master and doctor, considered to be one of the 100 most important scientists in the world, Marcos Pinotti was a foundation stone of our lab, leading us towards what we are today.</p>
									<footer>
										<ul class="buttons">
											<li><a href="pinotti.php" class="button">Meet Marcos Pinotti</a></li>
										</ul>
									</footer>

								</div>
								<div class="4u 12u(narrower) important(narrower)">


									<ul class="featured-icons">
										<li><span class="icon fa-assistive-listening-systems"><span class="label">Feature 1</span></span></li>
										<li><span class="icon fa-cog"><span class="label">Feature 2</span></span></li>
										<li><span class="icon fa-heartbeat"><span class="label">Feature 3</span></span></li>
										<li><span class="icon fa-laptop"><span class="label">Feature 4</span></span></li>
										<li><span class="icon fa-user-md"><span class="label">Feature 5</span></span></li>
										<li><span class="icon fa-medkit"><span class="label">Feature 6</span></span></li>
									</ul>

								</div>
							</div>
						</section>

						<section class="wrapper style3 container special">
							<div class="row">
								<div class="6u 12u(narrower)">

									<section>
										<header>
											<h3>OUR LAB</h3>
										</header>
										<p style="text-align: justify;">The lab was founded in 1999 and we have a lot of articles, research and products that can tell who we are. Our structure consists of <strong>1 meeting room</strong>,<strong> 1 room for presentations</strong>, <strong>3 workshops</strong>, <strong>2 computer rooms</strong> and <strong>1 room for the administration</strong>. Since the beggining, our lab is too much to be told on a webpage only. But, you can always come to us and talk!</p>
									</section>

									<a href="history.php" class="button">Our History</a>

								</div>
								<div class="6u 12u(narrower)">

									<section>
										<header>
											<h3>OUR TEAM</h3>
										</header>
										<p style="text-align: justify;"> Our team is composed by:
										2 <strong>Coordinators</strong>, 7<strong> PHD </strong>students, 15 <strong>master's degree </strong> students and 30 <strong> undergraduate students </strong>. Today, Rudolf Huebner leads the lab. He works in the areas of Heat and Fluids or Bioengineering that involve the evolution of mathematical models, numerical simulations and the use of non-invasive measurement techniques. </p>	
									</section>

									<a href="meet-the-team.php" class="button">Meet the whole team</a>

								</div>
							</div>
						</section>

						<header class="special container">

							<h2>Alone, we go faster...</h2>
							

							<span class="icon"><img id="handshake" src="assets/fonts/handshake.png" width="80" style="margin-top: -1%;"></span>
							<h2><strong>Together</strong>, we go <strong>further</strong>!</h2>
							<p style="width: 90%; display: block; margin: 0 auto; text-align: justify;">We can do so much more when we have the right partners! There's why we have choosen and being chosen by our associates along these years, to do things together, to change the world together... <strong>To support lives together...</strong> Meet our partners!	</p>

						</header>

						<section>
							<?php
								carousel("adm-sheets/parceiros.csv");
							?>
						</section> 

				</article>

			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>

	</body>
</html>