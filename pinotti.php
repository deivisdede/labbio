<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Marcos Pinotti - LABBIO UFMG</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>


			<!-- Main -->
				<article id="main">
						
					<!-- Three -->
					<section class="wrapper style3 container special">

						<header class="major">
							<h2>THERE IS SOME OF <strong>PINOTTI</strong>'S WORK</h2>
						</header>
<?php
					if (($handle = fopen("adm-sheets/pinotti-videos.csv", "r")) !== FALSE) {

						$indTitle = 0;
						$indLink = 2;
						$indDescription = 1;

						echo
						'<div class="row">';
						$row = 0;
						while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

							$row = $row+1;

							echo
							'<div class="6u 12u(narrower)">
								<section>
									<iframe width="320" heigth="250" src="'.str_replace("watch?v=", "embed/",$data[$indLink]).'" frameborder="0" allowfullscreen></iframe>
									<header>
										<h3 style="font-size: 0.9em;">'.$data[$indTitle].'</h3>
									</header>
									<p style="font-size: 80%; text-align: justify;"> &emsp; '.$data[$indDescription].'</p>
								</section>
							</div>';

							if($row % 2 == 0){
								echo 
								'</div>
									<div class="row">';
							}

						}
						echo
						'</div>';
					}
?>


					</section>

			</article>

			<!-- Footer -->
				<?php
					include "footer.html"
				?>

		</div>


	</body>
</html>