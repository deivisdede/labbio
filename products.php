<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Our Products</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>

			<!-- Main -->
				<article id="main">

					<header class="special container">
						<span class="icon fa-laptop"></span>
						<h2><strong>&nbsp&nbsp Development...</strong></h2>
						<h2>... and <strong> Improvement</strong>
					</header>

					<!-- One -->
						<section class="wrapper style4 container">

								<div class="10u 12u(narrower) important(narrower)" style="margin: 0 auto; display: block;">

									<!-- Content -->
										<div class="content 10u" style="margin: 0 auto; display: block;">
											<section>
												<table>
												
													<tbody>
													<?php


											if (($handle = fopen("./adm-sheets/products.csv", "r")) !== FALSE) {
												$indProduct = 0;
												$indYear = 1;
												$i = 0;
												while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
													$i++;
														echo '
														<tr>
															<td style="padding: 5px 30px 5px 5px;"><strong style="font-weight: 900">'
															.$i.'.</strong> '.$data[$indProduct].	
															'</td>
															<td>'
															.$data[$indYear].
															'</td>
														</tr>';
												}
												fclose($handle);
 											}
										?>
													</tbody>
												</table>
											</section>
										</div>

								</div>
							</div>
						</section>

					
				</article>

			<!-- Footer -->
				<?php
					include "footer.html"
				?>
				

		</div>


	</body>
</html>