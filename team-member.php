<!DOCTYPE HTML>
<!--
	Twenty by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<?php
	$nome = isset($_GET['nome']) ? $_GET['nome']: '';
?>
<html>
	<head>
		<title>Our Team - LABBIO</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	</head>
	<body class="no-sidebar">
		<div id="page-wrapper">

			<!-- Header -->
				<?php
					include 'script.html'
				?>
				
				<?php
					include 'header.html'
				?>

			<!-- Main -->
				<article id="main">
					<div style="display: block; margin: 0 auto;">
						<span style="display: block; margin: 2% auto; font-size: 120%; text-transform: uppercase; text-align: center;"><strong><?php echo $nome?></strong></span>
					</div>
					<!-- One -->

						<section class="wrapper style4 container">

							<!-- Content -->
								<div class="content" style="width: 90%; padding: 0; margin: 0 auto;">
									<section style="width: 100%; padding: 0; margin: 0 auto;">
										<?php
											team_member($nome);
										?>
										<a style = "display: block; margin: 5% auto; width: 40%;" href="meet-the-team.php" class="button">MEET THE WHOLE TEAM</a>

									</section>
								</div>

						</section>


				</article>

			<!-- Footer -->
				<?php
				/*
					include "footer.html"
				*/
				?>

		</div>
<?php
	function team_member($nome){

		$indNome = 3;
		$indVinculo = 1;
		$indImg = 8;
		$indCargo = 5;
		$indAtuacao1 = 6;
		$indAtuacao2 = 7;
		$indPesquisa = 2;
		$indMail = 4;
		$indLattes = 9;
		$indLinkedIn = 10;


		if (($handle = fopen("./adm-sheets/team.csv", "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		        $num = count($data);
		            
			        
			        if($nome === $data[$indNome]){

			        echo "<div class='div-member'>";
			            
			            echo "<img class='img-team' style='margin-bottom: 4%; width:180px!important; height: 180px!important;' src='./profile_pictures/". $data[$indImg] ."'>";
			            echo "<p style='text-align: center; vertical-align: center;'> <strong>Posição: </strong>". $data[$indCargo] ."</p>";
			            echo "<p style='text-align: center; vertical-align: center;'> <strong>Vínculo: </strong>". $data[$indVinculo] ."</p>";
			            echo "<p style='text-align: center; vertical-align: center;'> <strong>Áreas de atuação: </strong>". $data[$indAtuacao1];
				        if($data[$indAtuacao2] != "#"){
				            	echo " e ".$data[$indAtuacao2];
				            }
				            echo "</p>";
				        echo "<p style='text-align: center; vertical-align: center;'> <strong>Principal Projeto: </strong>". $data[$indPesquisa] ."</p>";
				        echo '
			            <ul class="icons" style="display: block; width: 40%; text-align: center; margin: 0 auto; font-size: 1.5em; font-size: 1.5vw;">
							<li><a href="mailto:'. $data[$indMail] .'" class="icon circle fa-envelope" id="icon-mail"><span class="label">Email</span></a></li>

							<li><a id="icon-lattes-team" href="' . $data[$indLattes] . '"><img class="icon circle fa-lattes" src="./images/fa-lattes.png"><span class="label" style="display:none;">Lattes</span></a></li>

							<li><a href="'. $data[$indLinkedIn] .'" class="icon circle fa-linkedin"><span class="label">LinkedIn</span></a></li>
						</ul>';
					echo "</div>";
					}

			    
		    }
		    fclose($handle);
		}
	}
?>
	</body>
</html>